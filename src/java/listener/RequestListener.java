/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package listener;

import bean.AppBean;
import bean.KorisnickiBean;
import javax.el.ELContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Edis
 */
public class RequestListener implements PhaseListener{

    @Override
    public void afterPhase(PhaseEvent pe) {
        HttpServletRequest hsr = (HttpServletRequest) pe.getFacesContext().getExternalContext().getRequest();
        String split[] = hsr.getRequestURI().split("/");
        String stranica = split[split.length - 1];
        ELContext fcCon = pe.getFacesContext().getELContext();
        AppBean app = (AppBean) fcCon.getELResolver().getValue(fcCon, null, "appBean");
        if (app.getZasticeneStranice().contains(stranica)){
            KorisnickiBean kb = (KorisnickiBean) fcCon.getELResolver().getValue(fcCon, null, "korisnickiBean");
            if (!kb.isPrijavljen())
            {
                System.out.println("Korisnik nije prijavljen");
                FacesContext fc = pe.getFacesContext();                
                pe.getFacesContext().getApplication().getNavigationHandler().handleNavigation(fc, null,"index");
                fc.renderResponse();
            }
            else if (kb.isPrijavljen()){
                    if (kb.getPrijavljeniKorisnik().isAdmin()){
                        if (app.getAdminStranice().contains(stranica))
                        {
                            FacesContext fc = pe.getFacesContext();                
                            pe.getFacesContext().getApplication().getNavigationHandler().handleNavigation(fc, null,"pocetna");
                            fc.renderResponse();  
                        }
                    }
                    else if (kb.getPrijavljeniKorisnik().isRadnik()){
                        if (app.getRadnikStranice().contains(stranica))
                        {
                            FacesContext fc = pe.getFacesContext();                
                            pe.getFacesContext().getApplication().getNavigationHandler().handleNavigation(fc, null,"radnikPocetna");
                            fc.renderResponse();
                        }
                    }
                    else if (kb.getPrijavljeniKorisnik().isKlijent()){
                        if (app.getKlijentStranice().contains(stranica)){
                            FacesContext fc = pe.getFacesContext();                
                            pe.getFacesContext().getApplication().getNavigationHandler().handleNavigation(fc, null,"klijentPocetna");
                            fc.renderResponse();
                        }
                    }
            }
            
        }
        
    }

    @Override
    public void beforePhase(PhaseEvent pe) {
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }
    
}
