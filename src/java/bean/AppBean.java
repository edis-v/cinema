/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import java.util.LinkedList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Edis
 */
@ManagedBean
@SessionScoped
public class AppBean {
    private List<String> klijentStranice = new LinkedList<String>();
    private List<String> adminStranice = new LinkedList<String>();
    private List<String> radnikStranice = new LinkedList<String>();
    private List<String> zasticeneStranice = new LinkedList<String>();
    
    public AppBean() {
    
        zasticeneStranice.add("administratori.jsf");
        zasticeneStranice.add("dodavanjeFilma.jsf");
        zasticeneStranice.add("filmovi.jsf");
        zasticeneStranice.add("finansije.jsf");
        zasticeneStranice.add("klijentPocetna.jsf");
        zasticeneStranice.add("klijenti.jsf");
        zasticeneStranice.add("pocetna.jsf");
        zasticeneStranice.add("prodajaKarata.jsf");
        zasticeneStranice.add("radnikPocetna.jsf");
        zasticeneStranice.add("rezervacije.jsf");
        zasticeneStranice.add("statistike.jsf");
        zasticeneStranice.add("zaposlenici.jsf");
        
        adminStranice.add("klijentPocetna.jsf");
        adminStranice.add("prodajaKarata.jsf");
        adminStranice.add("radnikPocetna.jsf");
        adminStranice.add("rezervacije.jsf");
      
        radnikStranice.add("administratori.jsf");
        radnikStranice.add("dodavanjeFilma.jsf");
        radnikStranice.add("finansije.jsf");
        radnikStranice.add("klijenti.jsf");
        radnikStranice.add("pocetna.jsf");
        radnikStranice.add("statistike.jsf");
        radnikStranice.add("zaposlenici.jsf");  
        radnikStranice.add("klijentPocetna.jsf");
        
        klijentStranice.add("administratori.jsf");
        klijentStranice.add("dodavanjeFilma.jsf");
        klijentStranice.add("finansije.jsf");
        klijentStranice.add("klijenti.jsf");
        klijentStranice.add("pocetna.jsf");
        klijentStranice.add("statistike.jsf");
        klijentStranice.add("zaposlenici.jsf");
        klijentStranice.add("prodajaKarata.jsf");
        klijentStranice.add("radnikPocetna.jsf");
        klijentStranice.add("rezervacije.jsf");
    
    }

    public List<String> getKlijentStranice() {
        return klijentStranice;
    }

    public void setKlijentStranice(List<String> klijentStranice) {
        this.klijentStranice = klijentStranice;
    }

    public List<String> getAdminStranice() {
        return adminStranice;
    }

    public void setAdminStranice(List<String> adminStranice) {
        this.adminStranice = adminStranice;
    }

    public List<String> getRadnikStranice() {
        return radnikStranice;
    }

    public void setRadnikStranice(List<String> radnikStranice) {
        this.radnikStranice = radnikStranice;
    }

    public List<String> getZasticeneStranice() {
        return zasticeneStranice;
    }

    public void setZasticeneStranice(List<String> zasticeneStranice) {
        this.zasticeneStranice = zasticeneStranice;
    }
    
    
    
}
