/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import baza.Film;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import konektor.MySqlKonektor;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Edis
 */
@ManagedBean
@ApplicationScoped
public class SlikaBean implements Serializable{
    private transient StreamedContent slika;
  // private List<Film> filmovi = new LinkedList<Film>();
    private Film film;
  InputStream is=null,input=null; 
 
 
   

    public SlikaBean() {
        /*
        ELContext fcCon = FacesContext.getCurrentInstance().getELContext();                
        FilmBean fb = (FilmBean) fcCon.getELResolver().getValue(fcCon, null, "filmBean");
        filmovi=fb.getFilmovi(); 
        System.out.println("Kostruktor SlikaBean");
        */
    }
    
  
    public StreamedContent getSlika() throws SQLException, IOException {
        
        
        System.out.println("Pozivanje getSlika");
        FacesContext context = FacesContext.getCurrentInstance();
        
        
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) 
        {
            System.out.println("Prvi put");
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        }
        else {
            // So, browser is requesting the image. Return a real StreamedContent with the image bytes.
            System.out.println("Drugi put");
              String id = context.getExternalContext().getRequestParameterMap().get("filmId");
             ucitajFilmove(id);
               
                    System.out.println("Ulazak u petlju"); 
                
                    if (film.getFilmId()==Integer.parseInt(id))
                    {
                        System.out.println("Ulazak u uslov. Trazeni  film je: "+film.getNazivFilma());
                   
                        if (film.getIs()!=null)
                        {
                            
                            is=film.getIs(); System.out.println("Punjenje input stream-a");
                            is.reset();
                            return new DefaultStreamedContent(is, "image/jpg");
                          
                        }
                            
                        else 
                        {
                            System.out.println("Null pointer");
                            
                            String url2="C:\\Users\\Edis\\Documents\\NetBeansProjects\\Kino\\web\\slike\\ni.png";
                            
                            input = new FileInputStream(url2);
                       
                            return new DefaultStreamedContent(input, "image/png");
                        } 
                        
                       
                    
                }
    }
        System.out.println("Kraj slika bean");
        return new DefaultStreamedContent();
    }
 

    public void setSlika(StreamedContent slika) {
        this.slika = slika;
    }

    
    public void ucitajFilmove(String id){
    
         try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return;
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              m.rs = m.stmt.executeQuery("SELECT * FROM filmovi WHERE film_id = "+id);
              
              Date v = new Date();
              
              while(m.rs.next()){
                  SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                  java.util.Date d = new Date(smf.parse(m.rs.getString(3)).getTime());
                  String s = smf.format(d);
                  //filmovi.add(new Film(m.rs.getInt(1),m.rs.getString(2), s ,m.rs.getInt(4), m.rs.getString(5),m.rs.getInt(6), m.rs.getBlob(7)));
                      film = new Film(m.rs.getInt(1),m.rs.getString(2), s ,m.rs.getInt(4), m.rs.getString(5),m.rs.getInt(6), m.rs.getBinaryStream(7));
                 
              }
              m.stmt.close();
              m.con.close();
           
              
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         
         return;
    }
    
    
}
