/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import baza.Korisnik;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.LinkedList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import konektor.MySqlKonektor;

/**
 *
 * @author Edis
 */
@ManagedBean
@SessionScoped
public class KlijentBean {
    private List<Korisnik>klijenti = new LinkedList<Korisnik>();
    Korisnik klijent;
    
    public KlijentBean() 
    {
        ucitajKlijente();
    }
    
    
    public String kreirajKlijenta()
    {
        klijent=new Korisnik();
        return null;
    }
    
    public String urediKlijenta(Korisnik k)
    {
        klijent=k;
        return null;
    }
    
    private String ucitajKlijente()
    {
            klijenti.clear();
         try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return null;
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              m.rs = m.stmt.executeQuery("SELECT * FROM korisnici WHERE klijent = 1");
              
              while(m.rs.next()){
                  klijenti.add(new Korisnik(m.rs.getInt(1),m.rs.getString(2), m.rs.getString(3), m.rs.getString(4),m.rs.getString(5), m.rs.getBoolean(6),  m.rs.getBoolean(7), m.rs.getBoolean(8)));
              }
              m.stmt.close();
              m.con.close();
             
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         
        return null;
    }
    
    public String snimiKlijenta(){
     try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch(Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Ne moze se izvršiti prijava","Ne može se izvršiti prijava"));
                    return null; 
        }
        MySqlKonektor m = new MySqlKonektor();
        try
        {
           m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
           if (klijent.getKorisnikId()==0)
           {
              m.ps = m.con.prepareStatement("INSERT INTO korisnici VALUES (?,?,?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
              m.ps.setInt(1, klijent.getKorisnikId());
              m.ps.setString(2, klijent.getIme());
              m.ps.setString(3, klijent.getPrezime());
              m.ps.setString(4, klijent.getKorisnickoIme());
              m.ps.setString(5, klijent.getLozinka());
              m.ps.setBoolean(6, false);
              m.ps.setBoolean(7, true);
              m.ps.setBoolean(8, false);
              m.ps.executeUpdate();
              m.rs = m.ps.getGeneratedKeys();
              m.rs.next();
              klijent.setKorisnikId(m.rs.getInt(1));
              m.rs.close();
              m.ps.close();
             
              klijenti.add(klijent);
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Uspješan unos","Uspješan unos"));
            
            }
            else
             {
               m.ps = m.con.prepareStatement("UPDATE korisnici SET ime = ?, prezime=?, korisnicko_ime=?, lozinka=?, radnik=?, klijent=?, admin=?  WHERE korisnik_id=?");
               m.ps.setString(1, klijent.getIme());
               m.ps.setString(2, klijent.getPrezime());
               m.ps.setString(3, klijent.getKorisnickoIme());
               m.ps.setString(4, klijent.getLozinka());
               m.ps.setBoolean(5, klijent.isRadnik());
               m.ps.setBoolean(6, klijent.isKlijent());
               m.ps.setBoolean(7, klijent.isAdmin());
               m.ps.setInt(8, klijent.getKorisnikId());
               m.ps.executeUpdate();
               m.ps.close();
               
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Uspješan unos","Napravljene su promjene u bazi"));

             }
           
        } catch (Exception ex) {
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Desila se greška","Desila se greška"));
        }
        
        return null;
    }
    
    
    public String obrisiKlijenta()
    {
            try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch(Exception ex) {
          FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Ne može se izvršiti prijava","Ne može se izvršiti prijava"));
           return null; 
        }
         MySqlKonektor m = new MySqlKonektor();
         try
         {
            m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
            m.stmt=m.con.createStatement();
            m.stmt.execute("DELETE FROM korisnici WHERE korisnik_id = "+klijent.getKorisnikId());
            m.stmt.close();
            m.con.close();
         }catch (Exception ex)
         {
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Desila se greška","Desila se greška"));
             return null;
         }
         klijenti.remove(klijent);
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Klijent je obrisan ","Klijent je obrisan"));

            return null;
    }
    
    public void postaviKlijentaZaBrisanje(Korisnik k)
    {
        klijent=k;
    }

    public List<Korisnik> getKlijenti() {
        return klijenti;
    }

    public void setKlijenti(List<Korisnik> klijenti) {
        this.klijenti = klijenti;
    }

    public Korisnik getKlijent() {
        return klijent;
    }

    public void setKlijent(Korisnik klijent) {
        this.klijent = klijent;
    }
    
    
}
