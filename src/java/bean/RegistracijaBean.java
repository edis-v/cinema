/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import baza.Korisnik;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import konektor.MySqlKonektor;

/**
 *
 * @author Edis
 */
@ManagedBean
@RequestScoped
public class RegistracijaBean {
    private Korisnik korisnik;
    
    public RegistracijaBean()
    {
        korisnik=new Korisnik();
    }
    
    public void registrujSe()
    {
         try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch(Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Ne moze se izvršiti prijava","Ne može se izvršiti prijava"));
                    return; 
        }
        MySqlKonektor m = new MySqlKonektor();
        try
        {
           m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
           if (korisnik.getKorisnikId()==0)
           {
              m.ps = m.con.prepareStatement("INSERT INTO korisnici VALUES (?,?,?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
              m.ps.setInt(1, korisnik.getKorisnikId());
              m.ps.setString(2, korisnik.getIme());
              m.ps.setString(3, korisnik.getPrezime());
              m.ps.setString(4, korisnik.getKorisnickoIme());
              m.ps.setString(5, korisnik.getLozinka());
              m.ps.setBoolean(6, false);
              m.ps.setBoolean(7, true);
              m.ps.setBoolean(8, false);
              m.ps.executeUpdate();
              m.rs = m.ps.getGeneratedKeys();
              m.rs.next();
              korisnik.setKorisnikId(m.rs.getInt(1));
              m.rs.close();
              m.ps.close();
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Uspješna registracija","Uspješna registracija"));
            
            }
            else
             {
               m.ps = m.con.prepareStatement("UPDATE korisnici SET ime = ?, prezime=?, korisnicko_ime=?, lozinka=?, radnik=?, klijent=?, admin=?  WHERE korisnik_id=?");
               m.ps.setString(1, korisnik.getIme());
               m.ps.setString(2, korisnik.getPrezime());
               m.ps.setString(3, korisnik.getKorisnickoIme());
               m.ps.setString(4, korisnik.getLozinka());
               m.ps.setBoolean(5, korisnik.isRadnik());
               m.ps.setBoolean(6, korisnik.isKlijent());
               m.ps.setBoolean(7, korisnik.isAdmin());
               m.ps.setInt(8, korisnik.getKorisnikId());
               m.ps.executeUpdate();
               m.ps.close();
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Uspješan unos","Napravljene su promjene u bazi"));

             }
           
        } catch (Exception ex) {
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Desila se greška","Desila se greška"));
        }
    }

    
    
    
    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }
    
    
}
