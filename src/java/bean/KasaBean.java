/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import baza.Kasa;
import baza.Korisnik;
import java.io.Serializable;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import konektor.MySqlKonektor;

/**
 *
 * @author Edis
 */
@ManagedBean
@SessionScoped
public class KasaBean implements Serializable{
    private Kasa kasa;
    private List<Kasa>dnevnaStanja = new LinkedList<Kasa>();

    
    public KasaBean() 
    {
        ucitajStanja();
        //traziPrethodnoStanje();
    }
    
    
    public String kreirajInstancu()
    {
        kasa=new Kasa();
        return null;
    }
    
    public String snimiKasu()
    {
        Date datum = new Date();
        java.sql.Date sqlDate = new java.sql.Date(datum.getTime());
        kasa.setDatum_zakljucivanja(datum);
        ELContext fcCon = FacesContext.getCurrentInstance().getELContext();                
        KorisnickiBean kb = (KorisnickiBean) fcCon.getELResolver().getValue(fcCon, null, "korisnickiBean");
        kasa.setZakljucio(kb.getPrijavljeniKorisnik().getKorisnikId());
        traziPrethodnoStanje(); //postavlja stanje kao u prethodne
        kasa.setStanje((kasa.getStanje()+kasa.getPrihod()-kasa.getRashod()));
          try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch(Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Ne moze se izvršiti prijava","Ne može se izvršiti prijava"));
                    return null ; 
        }
        MySqlKonektor m = new MySqlKonektor();
        try
        {
           m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
           if (kasa.getKasaId() == 0)
           {
           m.ps = m.con.prepareStatement("INSERT INTO kase VALUES (?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
           m.ps.setInt(1, kasa.getKasaId());
           m.ps.setInt(2, kasa.getStanje());
           m.ps.setInt(3, kasa.getPrihod());
           m.ps.setInt(4, kasa.getRashod());
           m.ps.setDate(5, sqlDate);
           m.ps.setInt(6, kasa.getZakljucio());
           m.ps.executeUpdate();
           m.rs = m.ps.getGeneratedKeys();
           m.rs.next();
           kasa.setKasaId(m.rs.getInt(1));
           m.rs.close();
           m.ps.close();
           dnevnaStanja.add(kasa);
           FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Uspješan unos","Kasa je zaključena"));

           }
           else
           {
               m.ps = m.con.prepareStatement("UPDATE kase SET stanje = ?, prihod=?, rashod=?, datum_zakljucivanja=?, zakljucio=?  WHERE kasa_id=?");
               m.ps.setInt(1, kasa.getStanje());
           m.ps.setInt(2, kasa.getPrihod());
           m.ps.setInt(3, kasa.getRashod());
           m.ps.setDate(4, sqlDate);
           m.ps.setInt(5, kasa.getZakljucio());
           m.ps.setInt(6, kasa.getKasaId());
               m.ps.executeUpdate();
               m.ps.close();
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Uspješan unos","Napravljene su promjene u bazi"));

           }
           m.con.close();
        }  catch (Exception ex){ 
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Desila se greška","Desila se greška"));

            }
        
        return null;
    }
    
    private void traziPrethodnoStanje()
    {
        int x = dnevnaStanja.size();
        if (x>=1){
        Kasa k = dnevnaStanja.get(x-1);
        kasa.setStanje(k.getStanje());
        }
        else {
            kasa.setStanje(0);
        }
        
    }
    
    
    private void ucitajStanja()
    {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return;
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              m.rs = m.stmt.executeQuery("SELECT * FROM kase");
              
              while(m.rs.next()){
                  dnevnaStanja.add(new Kasa (m.rs.getInt(1),m.rs.getInt(2), m.rs.getInt(3),m.rs.getInt(4), m.rs.getDate(5), m.rs.getInt(6)));
              }
              m.stmt.close();
              m.con.close();
              
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         
        return;
    }
    
    
    public String urediDatum(Date d)
    {
        SimpleDateFormat smf = new SimpleDateFormat("yyyy-dd-MM");
        return smf.format(d);
    }
    
    public String kasuJeZakljucio(int id) throws SQLException
    {
         Korisnik k = null;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return null;
        }
         MySqlKonektor m = new MySqlKonektor();
        try {
           
           m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
            m.stmt = m.con.createStatement();
            m.rs = m.stmt.executeQuery("SELECT * from korisnici WHERE korisnik_id='" + id + "'");
            while (m.rs.next()) {
                    k = new Korisnik(m.rs.getInt(1), m.rs.getString(2), m.rs.getString(3), m.rs.getString(4), m.rs.getString(5), m.rs.getBoolean(6), m.rs.getBoolean(7),m.rs.getBoolean(8));
                  break;
                }
            } catch (Exception ex) {}
            m.stmt.close();
            m.con.close();
            
            String zakljucioc = k.getIme()+" "+k.getPrezime();
            return zakljucioc;
    } 
        
    
    
    
    
    
    public Kasa getKasa() {
        return kasa;
    }

    public void setKasa(Kasa kasa) {
        this.kasa = kasa;
    }

    public List<Kasa> getDnevnaStanja() {
        return dnevnaStanja;
    }

    public void setDnevnaStanja(List<Kasa> dnevnaStanja) {
        this.dnevnaStanja = dnevnaStanja;
    }
    
    
}
