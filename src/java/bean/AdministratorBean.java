/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import baza.Korisnik;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.LinkedList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import konektor.MySqlKonektor;

/**
 *
 * @author Edis
 */
@ManagedBean
@SessionScoped
public class AdministratorBean {
     private List<Korisnik> administratori = new LinkedList<Korisnik>();
     private Korisnik administrator;
    
    
     public AdministratorBean(){
        ucitajAdministratore();
    }
    
    
    
    private String ucitajAdministratore()
    {
        administratori.clear();
         try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return null;
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              m.rs = m.stmt.executeQuery("SELECT * FROM korisnici WHERE admin=1");
              
              while(m.rs.next()){
                  administratori.add(new Korisnik(m.rs.getInt(1),m.rs.getString(2), m.rs.getString(3), m.rs.getString(4),m.rs.getString(5), m.rs.getBoolean(6),  m.rs.getBoolean(7), m.rs.getBoolean(8)));
              }
              m.stmt.close();
              m.con.close();
             
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         
        return null;
    }
    
    public String kreirajAdministratora()
    {
        administrator=new Korisnik();
        return null;
    }
    
    public String snimiAdministratora()
    {
         try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch(Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Ne moze se izvršiti prijava","Ne može se izvršiti prijava"));
                    return null; 
        }
        MySqlKonektor m = new MySqlKonektor();
        try
        {
           m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
           if (administrator.getKorisnikId()==0)
           {
              m.ps = m.con.prepareStatement("INSERT INTO korisnici VALUES (?,?,?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
              m.ps.setInt(1, administrator.getKorisnikId());
              m.ps.setString(2, administrator.getIme());
              m.ps.setString(3, administrator.getPrezime());
              m.ps.setString(4, administrator.getKorisnickoIme());
              m.ps.setString(5, administrator.getLozinka());
              m.ps.setBoolean(6, false);
              m.ps.setBoolean(7, false);
              m.ps.setBoolean(8, true);
              m.ps.executeUpdate();
              m.rs = m.ps.getGeneratedKeys();
              m.rs.next();
              administrator.setKorisnikId(m.rs.getInt(1));
              m.rs.close();
              m.ps.close();
              administratori.add(administrator);
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Uspješan unos","Uspješan unos"));
            
            }
            else
             {
               m.ps = m.con.prepareStatement("UPDATE korisnici SET ime = ?, prezime=?, korisnicko_ime=?, lozinka=?, radnik=?, klijent=?, admin=?  WHERE korisnik_id=?");
               m.ps.setString(1, administrator.getIme());
               m.ps.setString(2, administrator.getPrezime());
               m.ps.setString(3, administrator.getKorisnickoIme());
               m.ps.setString(4, administrator.getLozinka());
               m.ps.setBoolean(5, administrator.isRadnik());
               m.ps.setBoolean(6, administrator.isKlijent());
               m.ps.setBoolean(7, administrator.isAdmin());
               m.ps.setInt(8, administrator.getKorisnikId());
               m.ps.executeUpdate();
               m.ps.close();
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Uspješan unos","Napravljene su promjene u bazi"));

             }
           
        } catch (Exception ex) {
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Desila se greška","Desila se greška"));
        }
        return null;
    }
    
    
    public String urediAdministratora(Korisnik z)
    {
        administrator=z; 
        return null;
    }
    
    public String obrisiAdministratora()
    {
            try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch(Exception ex) {
          FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Ne može se izvršiti prijava","Ne može se izvršiti prijava"));
           return null; 
        }
         MySqlKonektor m = new MySqlKonektor();
         try
         {
            m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
            m.stmt=m.con.createStatement();
            m.stmt.execute("DELETE FROM korisnici WHERE korisnik_id = "+administrator.getKorisnikId());
            m.stmt.close();
            m.con.close();
         }catch (Exception ex)
         {
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Desila se greška","Desila se greška"));
             return null;
         }
         administratori.remove(administrator);
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Zaposlenik je obrisan ","Zaposlenik je obrisan"));

            return null;
    }
    
    public void postaviAdministratoraZaBrisanje(Korisnik z)
    {
        administrator=z; 
    }

    public List<Korisnik> getAdministratori() {
        return administratori;
    }

    public void setAdministratori(List<Korisnik> administratori) {
        this.administratori = administratori;
    }

    public Korisnik getAdministrator() {
        return administrator;
    }

    public void setAdministrator(Korisnik administrator) {
        this.administrator = administrator;
    }
    
    
    
}
