/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import baza.Film;
import baza.Sala;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import konektor.MySqlKonektor;
import org.primefaces.event.FileUploadEvent;


/**
 *
 * @author Edis
 */
@ManagedBean
@SessionScoped
public class FilmBean implements Serializable{
    private transient List<Film> filmovi = new LinkedList<Film>();
    private transient Film film;
    private transient InputStream is;
    private Date datum;
    private Sala salaPrikazivanja;
   


    
    public FilmBean ()
    {
        film=new Film();
        ucitajFilmove(); System.out.println("Konstruktor FilmBean");
        film.setZanr("Akcijski");
        salaPrikazivanja = new Sala();
        salaPrikazivanja.setBrojSale(1);
    
    }
    
    public String prebaciNaFilmove()
    {
        ucitajFilmove(); System.out.println("UPDATE liste filmova");
        return "filmovi?faces-redirect=true";   
    }
    
    
    public void listenerZanraFilma()
    {
        System.out.println("Zanr filma je: "+film.getZanr());
    }
    
    
    
     public void upload(FileUploadEvent event) {  
        FacesMessage msg = new FacesMessage("Uspješan upload! ", event.getFile().getFileName() + " je upload-an.");  
        FacesContext.getCurrentInstance().addMessage(null, msg);
        // Do what you want with the file        
        try {
            is = event.getFile().getInputstream();
            
        } catch (IOException ex) {
            Logger.getLogger(FilmBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        film.setIs(is);
     }
        
   
     public String snimiFilm()
     {
          try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch(Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Ne moze se izvršiti prijava","Ne može se izvršiti prijava"));
                    return null; 
        }
        MySqlKonektor m = new MySqlKonektor();
        try
        {
           m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
           if (film.getFilmId() == 0)
           {
               //java.sql.Date sqlDate = new java.sql.Date(film.getDatum().getTime()); 
               datum=new java.sql.Date(datum.getTime());
               SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
               String dt = smf.format(datum);
               film.setDatum(dt);
               film.setSala(salaPrikazivanja.getSalaId());
              // film.setIs(is); 
           m.ps = m.con.prepareStatement("INSERT INTO filmovi VALUES (?,?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
           m.ps.setInt(1, film.getFilmId());
           m.ps.setString(2, film.getNazivFilma()); 
           m.ps.setString(3, dt);
           m.ps.setInt(4, film.getCijena());   
           m.ps.setString(5, film.getZanr());
           m.ps.setInt(6, film.getSala());
           m.ps.setBlob(7, film.getIs()); 
           m.ps.executeUpdate();
           m.rs = m.ps.getGeneratedKeys();
           m.rs.next();
           film.setFilmId(m.rs.getInt(1));
           m.rs.close();
           m.ps.close();
           filmovi.add(film);
           
           FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Uspješan unos","Film je uspješno dodan"));

           }
           else
           { System.out.println("UPDATE filma");
                datum=new java.sql.Date(datum.getTime());
               SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
               String dt = smf.format(datum);
               
               m.ps = m.con.prepareStatement("UPDATE filmovi SET naziv_filma = ?, datum_prikazivanja=?, cijena_ulaznice=?, zanr=?, sala=?, poster=?  WHERE film_id=?");
               m.ps.setString(1, film.getNazivFilma());
               m.ps.setString(2, dt);
               m.ps.setInt(3, film.getCijena());
               m.ps.setString(4, film.getZanr());
               m.ps.setInt(5, film.getSala());
               m.ps.setBlob(6, film.getIs());
               m.ps.setInt(7, film.getFilmId());
               m.ps.executeUpdate();
               m.ps.close();
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Uspješan unos","Napravljene su promjene u bazi"));

           }
           m.con.close();
        }  catch (Exception ex){ 
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Desila se greška","Desila se greška"));

            return null;}
       film = new Film();
       datum=null;
        return null;
     }
     
     public String ucitajFilmove()
     {
         filmovi.clear();
         try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return null;
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              m.rs = m.stmt.executeQuery("SELECT * FROM filmovi");
              
              Date v = new Date();
              
              while(m.rs.next()){
                  SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                  java.util.Date d = new Date(smf.parse(m.rs.getString(3)).getTime());
                  
                  if (v.before(d))
                  {
                      System.out.println("Usao u if uslov");
                      SimpleDateFormat smf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                      String s = smf1.format(d);
                  //filmovi.add(new Film(m.rs.getInt(1),m.rs.getString(2), s ,m.rs.getInt(4), m.rs.getString(5),m.rs.getInt(6), m.rs.getBlob(7)));
                      filmovi.add(new Film(m.rs.getInt(1),m.rs.getString(2), s ,m.rs.getInt(4), m.rs.getString(5),m.rs.getInt(6), m.rs.getBinaryStream(7)));
                  }
                  else System.out.println("Film je vec bio");
              }
              m.stmt.close();
              m.con.close();
              System.out.println("Duzina liste je: "+filmovi.size());
              
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         
         return null;
     }
    
 

        public void listener()
        {
            film.setSala(salaPrikazivanja.getSalaId());
            System.out.println("Odabrana je sala broj: "+salaPrikazivanja.getBrojSale());
        }
        
      
        public String urediDatum(String s)
        {
          
            System.out.println("Dobiveni datum je: "+s);
            int x = s.lastIndexOf(":"); System.out.println("Zadnje : je: "+x);
            String novi = s.substring(0, x);
            novi=novi+"h";
            return novi;
            
        }
        
        public String obrisiFilm()
        {
             try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch(Exception ex) {
          FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Ne može se izvršiti prijava","Ne može se izvršiti prijava"));
           return null; 
        }
         MySqlKonektor m = new MySqlKonektor();
         try
         {
            m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
            m.stmt=m.con.createStatement();
            m.stmt.execute("DELETE FROM filmovi WHERE film_id = "+film.getFilmId());
            m.stmt.close();
            m.con.close();
         }catch (Exception ex)
         {
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Desila se greška","Desila se greška"));
             return null;
         }
         filmovi.remove(film);
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Film je uspješno obrisan","Film je uspješno obrisan"));

         return null;
        
        }
        
        public String podesiFilmZaBrisanje(Film f)
        {
            film=f; System.out.println("Podešen je film za brisanje: "+f.getNazivFilma());
            return null;
        }
        
        public String pripremiZaUredjivanje(Film f) throws ParseException
        {
            film=f;
            film.setFilmId(f.getFilmId());
            film.setNazivFilma(f.getNazivFilma());
            SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            datum = smf.parse(f.getDatum());
             System.out.println("Datum u pripremiZredjivanje : "+datum);
            film.setCijena(f.getCijena());
            film.setZanr(f.getZanr());
            film.setSala(f.getSala());
            film.setIs(f.getIs());
            
             try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              System.out.println("ID sale je: "+f.getSala());
              m.rs = m.stmt.executeQuery("SELECT * FROM sale WHERE sala_id ='" +f.getSala()+"'");
              while(m.rs.next()){
                  salaPrikazivanja=new Sala(m.rs.getInt(1),m.rs.getInt(2), m.rs.getInt(3));
              }
              m.stmt.close();
              m.con.close();
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
            return "dodavanjeFilma?faces-redirect=true";
        }
        
        
        
        
    public InputStream getIs() {
        return is;
    }

    public void setIs(InputStream is) {
        this.is = is;
    }
        
    
    public List<Film> getFilmovi() {
        return filmovi;
    }

    public void setFilmovi(List<Film> filmovi) {
        this.filmovi = filmovi;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Sala getSalaPrikazivanja() {
        return salaPrikazivanja;
    }

    public void setSalaPrikazivanja(Sala salaPrikazivanja) {
        this.salaPrikazivanja = salaPrikazivanja;
    }
    
    
    
    
    
}
