/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import baza.Film;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import konektor.MySqlKonektor;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

/**
 *
 * @author Edis
 */
@ManagedBean
@SessionScoped
public class StatistikaBean {
    private Date pocetak,kraj;
    private int neto,bruto,rashodi;
    private String godina;
    private CartesianChartModel model= new CartesianChartModel();
     private int m1=0,m2=0,m3=0,m4=0,m5=0,m6=0,m7=0,m8=0,m9=0,m10=0,m11=0,m12=0;
    private boolean prikaziGraf=false;
    private boolean prikaziIspis=false;
    private boolean prikaziTabelu=false;
    private  List<Film> filmovi = new LinkedList<Film>();
    
    public StatistikaBean() {
    ucitajFilmove();
    }
    
    
    public String prikaziGraf()
    {
        prikaziIspis=false;
        System.out.println("Pocetak prikazi graf");
        int x = Integer.parseInt(godina);
        Date d1,d2; //pocetak i kraj godine
        Calendar c = new GregorianCalendar();
        c.set(Calendar.DATE, 1);
        c.set(Calendar.MONTH, 0);
        c.set(Calendar.YEAR, x);
        c.set(Calendar.HOUR_OF_DAY, 1);
        c.set(Calendar.MINUTE, 1);
        d1=c.getTime(); 
        
        c.set(Calendar.DATE, 31);
        c.set(Calendar.MONTH, 11);
        c.set(Calendar.YEAR, x);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        d2=c.getTime(); 
        
        SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd");
        String s1=smf.format(d1);
        String s2=smf.format(d2);
        model=new CartesianChartModel(); System.out.println("Kreirana nova instanca grafa");
                m1=0;m2=0;m3=0;m4=0;m5=0;m6=0;m7=0;m8=0;m9=0;m10=0;m11=0;m12=0;
        
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return null;
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              m.rs = m.stmt.executeQuery("SELECT * FROM kase WHERE datum_zakljucivanja > '"+s1+"' AND datum_zakljucivanja < '"+s2+"' ");
              
              while(m.rs.next()){
                  String d = smf.format(m.rs.getDate(5));
                  System.out.println(d);
                  puniGraf(m.rs.getInt(3),d);
              }
              m.stmt.close();
              m.con.close();
              
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
        
        iscrtajGraf(); System.out.println("Graf je iscrtan");
        prikaziGraf=true; System.out.println("Renderovanje true");
        return null;
    }
    
    
    private void puniGraf(int novac ,String s)
    {
            String mjesec = s.substring(5,7);
            
            if (mjesec.equalsIgnoreCase("01"))
            {
                m1=m1+novac;
            }
            else if (mjesec.equalsIgnoreCase("02"))
            {
                m2=m2+novac;
            }
            else if (mjesec.equalsIgnoreCase("03"))
            {
                m3=m3+novac;
            }
            else if (mjesec.equalsIgnoreCase("04"))
            {
                m4=m4+novac;
            }
            else if (mjesec.equalsIgnoreCase("05"))
            {
                m5=m5+novac;
            }
            else if (mjesec.equalsIgnoreCase("06"))
            {
                m6=m6+novac;
            }
            else if (mjesec.equalsIgnoreCase("07"))
            {
                m7=m7+novac;
            }
            else if (mjesec.equalsIgnoreCase("08"))
            {
                m8=m8+novac;
            }
            else if (mjesec.equalsIgnoreCase("09"))
            {
                m9=m9+novac;
            }
            else if (mjesec.equalsIgnoreCase("10"))
            {
                m10=m10+novac;
            }
            else if (mjesec.equalsIgnoreCase("11"))
            {
                m11=m11+novac;
            }
            else if (mjesec.equalsIgnoreCase("12"))
            {
                m12=m12+novac;
            }
    }
    
    private void iscrtajGraf()
    {
        
        ChartSeries m = new ChartSeries("Semestri");
        m.set("1", m1);
        m.set("2", m2);
        m.set("3", m3);
        m.set("4", m4);
        m.set("5", m5);
        m.set("6", m6);
        m.set("7", m7);
        m.set("8", m8);
        m.set("9", m9);
        m.set("10", m10);
        m.set("11", m11);
        m.set("12", m12);
        
        model.addSeries(m);
    }
    
    public String prikaziStatistiku()
    {
        prikaziGraf=false;
        SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd");
        String p = smf.format(pocetak); System.out.println(p);
        String k = smf.format(kraj); System.out.println(k);
        neto=0; bruto=0; rashodi=0;
        
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return null;
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              m.rs = m.stmt.executeQuery("SELECT * FROM kase WHERE datum_zakljucivanja > '"+p+"' AND datum_zakljucivanja < '"+k+"' ");
              
              while(m.rs.next()){
         
                  neto = neto  + m.rs.getInt(3);
                  rashodi = rashodi + m.rs.getInt(4);
              }
              m.stmt.close();
              m.con.close();
              
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         bruto = neto+rashodi;
        prikaziIspis=true;
      
        return null;
    }

    
    public String ucitajFilmove()
    {
      
         filmovi.clear();
         try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return null;
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              m.rs = m.stmt.executeQuery("SELECT * FROM filmovi");
              while (m.rs.next()){
                  //filmovi.add(new Film(m.rs.getInt(1),m.rs.getString(2), s ,m.rs.getInt(4), m.rs.getString(5),m.rs.getInt(6), m.rs.getBlob(7)));
                      filmovi.add(new Film(m.rs.getInt(1),m.rs.getString(2), null ,m.rs.getInt(4), null , 0 , null));
              }
              m.stmt.close();
              m.con.close();
         
         }
         
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         //prikaziIspis=true;
         prikaziTabelu=true;
         return null;
    }
    
    public int racunajZaradu (int id,int cijena)
    {
        int brojKarti=0;
         try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return 0;
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              m.rs = m.stmt.executeQuery("SELECT COUNT(*) FROM karte WHERE film_id = "+id);
              
              while(m.rs.next()){
                  brojKarti=m.rs.getInt(1); 
              }
              m.stmt.close();
              m.con.close();
            
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         return (brojKarti*cijena);
    }
    
    public Date getPocetak() {
        return pocetak;
    }

    public void setPocetak(Date pocetak) {
        this.pocetak = pocetak;
    }

    public Date getKraj() {
        return kraj;
    }

    public void setKraj(Date kraj) {
        this.kraj = kraj;
    }

    public int getNeto() {
        return neto;
    }

    public void setNeto(int neto) {
        this.neto = neto;
    }

    public int getBruto() {
        return bruto;
    }

    public void setBruto(int bruto) {
        this.bruto = bruto;
    }

    public int getRashodi() {
        return rashodi;
    }

    public void setRashodi(int rashodi) {
        this.rashodi = rashodi;
    }

    public String getGodina() {
        return godina;
    }

    public void setGodina(String godina) {
        this.godina = godina;
    }

    public CartesianChartModel getModel() {
        return model;
    }

    public void setModel(CartesianChartModel model) {
        this.model = model;
    }

    public boolean isPrikaziGraf() {
        return prikaziGraf;
    }

    public void setPrikaziGraf(boolean prikaziGraf) {
        this.prikaziGraf = prikaziGraf;
    }

    public boolean isPrikaziIspis() {
        return prikaziIspis;
    }

    public void setPrikaziIspis(boolean prikaziIspis) {
        this.prikaziIspis = prikaziIspis;
    }

    public List<Film> getFilmovi() {
        return filmovi;
    }

    public void setFilmovi(List<Film> filmovi) {
        this.filmovi = filmovi;
    }

    public boolean isPrikaziTabelu() {
        return prikaziTabelu;
    }

    public void setPrikaziTabelu(boolean prikaziTabelu) {
        this.prikaziTabelu = prikaziTabelu;
    }
    
    
    
}
