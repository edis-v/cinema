/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import baza.Korisnik;
import java.sql.DriverManager;
import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import konektor.MySqlKonektor;

/**
 *
 * @author Edis
 */
@ManagedBean
@SessionScoped
public class LoginBean {
    private String korisnickoIme,lozinka;

    
    public String prijaviSe()
    {
         try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return null;
        }
         MySqlKonektor m = new MySqlKonektor();
        try {
            Korisnik k = null;
           m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
            m.stmt = m.con.createStatement();
            m.rs = m.stmt.executeQuery("SELECT * from korisnici WHERE korisnicko_ime='" + korisnickoIme + "'");
            while (m.rs.next()) {
                String ki = m.rs.getString("korisnicko_ime"); //selekcija vrijednosti iz ResultSet-a prema imenu/aliasa kolone
                String loz = m.rs.getString(5); //selekcija vrijednosti iz ResultSet-a prema indeksu kolone (indeks prve kolone je 1, itd.)
                if (korisnickoIme.equals(ki) && lozinka.equals(loz)) {
                    k = new Korisnik(m.rs.getInt(1), m.rs.getString(2), m.rs.getString(3), ki, loz, m.rs.getBoolean(6), m.rs.getBoolean(7),m.rs.getBoolean(8));
                    break;
                }
            }
            m.stmt.close();
            m.con.close();
            if (k == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Korisnički podaci nisu validni.", "Korisnički podaci nisu validni."));
                return null;
            } else {
                ELContext fcCon = FacesContext.getCurrentInstance().getELContext();                
                KorisnickiBean kb = (KorisnickiBean) fcCon.getELResolver().getValue(fcCon, null, "korisnickiBean"); //pribavljanje bean instance "korisnickiBean" (ukoliko instanca ne postoji, ova instanca će biti kreirana i smještena u sesijski kontekst)
                kb.setPrijavljeniKorisnik(k);
                kb.setPrijavljen(true);
                if (kb.getPrijavljeniKorisnik().isAdmin())
                {
                    return "pocetna?faces-redirect=true";
                }
                else if (kb.getPrijavljeniKorisnik().isRadnik())
                {
                    return "radnikPocetna?faces-redirect=true";
                }
                else if (kb.getPrijavljeniKorisnik().isKlijent())
                {
                    return "klijentPocetna?faces-redirect=true";
                }
                return null;
                
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Desila se greška", "Desila se greška."));
            return null;
        }
    }
   
    
    public String odjaviSe()
    {
        ELContext fcCon = FacesContext.getCurrentInstance().getELContext();                
                KorisnickiBean kb = (KorisnickiBean) fcCon.getELResolver().getValue(fcCon, null, "korisnickiBean"); //pribavljanje bean instance "korisnickiBean" (ukoliko instanca ne postoji, ova instanca će biti kreirana i smještena u sesijski kontekst)
                kb.setPrijavljeniKorisnik(null);
                kb.setPrijavljen(false);
                return "index?faces-redirect=true";
    }    
    
    public String getKorisnikoIme() {
        return korisnickoIme;
    }

    public void setKorisnikoIme(String korisnikoIme) {
        this.korisnickoIme = korisnikoIme;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }
    
    
    
}
