/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import baza.Sala;
import java.io.Serializable;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.LinkedList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import konektor.MySqlKonektor;

/**
 *
 * @author Edis
 */
@ManagedBean
@SessionScoped
public class SalaBean implements Serializable{
    private List<Sala> sale = new LinkedList<Sala>();
    private Sala odabranaSala;
    private Sala sala;
    private boolean prikaziSale=false;
    private boolean prikaziUredjivanje=false;
    private Sala salaBrisanje;
    
    
    public SalaBean() {
        ucitajSale(0);
    }
    
    
    public String kreirajSalu()
    {
        sala = new Sala();
        prikaziUredjivanje=true;
        return null;
    }
    
    public String urediSalu(Sala s)
    {
        sala=s;
        prikaziUredjivanje=true;
        return null;
    }
    
    public String odustani()
    {
        prikaziUredjivanje=false;
        return null;
    }
    
    public String ucitajSale(int i)
    {
        sale.clear();
         try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return null;
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              m.rs = m.stmt.executeQuery("SELECT * FROM sale");
              
              while(m.rs.next()){
                  sale.add(new Sala(m.rs.getInt(1),m.rs.getInt(2), m.rs.getInt(3)));
              }
              m.stmt.close();
              m.con.close();
              
              if (i==1)
              {
              prikaziSale=true;  System.out.println("Renderovanje je na true");
              }
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         
        return null;
    }
    
    public String snimiSalu()
    {
           try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch(Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Ne moze se izvršiti prijava","Ne može se izvršiti prijava"));
                    return null; 
        }
        MySqlKonektor m = new MySqlKonektor();
        try
        {
           m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
           if (sala.getSalaId() == 0)
           {
           m.ps = m.con.prepareStatement("INSERT INTO sale VALUES (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
           m.ps.setInt(1, sala.getSalaId());
           m.ps.setInt(2, sala.getBrojSale());
           m.ps.setInt(3, sala.getBrojMjesta());
           m.ps.executeUpdate();
           m.rs = m.ps.getGeneratedKeys();
           m.rs.next();
           sala.setSalaId(m.rs.getInt(1));
           m.rs.close();
           m.ps.close();
           sale.add(sala);
           FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Uspješan unos","Sala je uspješno dodana"));

           }
           else
           {
               m.ps = m.con.prepareStatement("UPDATE sale SET broj_sale = ?, broj_mjesta=?  WHERE sala_id=?");
               m.ps.setInt(1, sala.getBrojSale());
               m.ps.setInt(2, sala.getBrojMjesta());
               m.ps.setInt(3, sala.getSalaId());
               m.ps.executeUpdate();
               m.ps.close();
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Uspješan unos","Napravljene su promjene u bazi"));

           }
           m.con.close();
        }  catch (Exception ex){ 
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Desila se greška","Desila se greška"));

            return null;}
        
        prikaziUredjivanje=false; System.out.println("Renderovanje je na false");
        return null;
    }
    
    public String obrisiSalu()
    {
         try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch(Exception ex) {
          FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Ne može se izvršiti prijava","Ne može se izvršiti prijava"));
           return null; 
        }
         MySqlKonektor m = new MySqlKonektor();
         try
         {
            m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
            m.stmt=m.con.createStatement();
            m.stmt.execute("DELETE FROM sale WHERE sala_id = "+salaBrisanje.getSalaId());
            m.stmt.close();
            m.con.close();
         }catch (Exception ex)
         {
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Desila se greška","Desila se greška"));
             return null;
         }
         sale.remove(salaBrisanje);
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Sala je uspješno obrisana","Sala je uspješno obrisana"));

         return null;
        
    }
    
    public String zatvoriProzore()
    {
        prikaziSale=false;
        prikaziUredjivanje=false;
        return null;
    }
    
    public void postaviIdSaleZaBrisanje(Sala s)
    {
        salaBrisanje=s;
    }
    

    public List<Sala> getSale() {
        return sale;
    }

    public void setSale(List<Sala> sale) {
        this.sale = sale;
    }

    public Sala getOdabranaSala() {
        return odabranaSala;
    }

    public void setOdabranaSala(Sala odabranaSala) {
        this.odabranaSala = odabranaSala;
    }

    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

    public boolean isPrikaziSale() {
        return prikaziSale;
    }

    public void setPrikaziSale(boolean prikaziSale) {
        this.prikaziSale = prikaziSale;
    }

    public boolean isPrikaziUredjivanje() {
        return prikaziUredjivanje;
    }

    public void setPrikaziUredjivanje(boolean prikaziUredjivanje) {
        this.prikaziUredjivanje = prikaziUredjivanje;
    }

    public Sala getSalaBrisanje() {
        return salaBrisanje;
    }

    public void setSalaBrisanje(Sala salaBrisanje) {
        this.salaBrisanje = salaBrisanje;
    }

 
    
    
    
}
