/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import baza.Korisnik;
import java.sql.DriverManager;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import konektor.MySqlKonektor;

/**
 *
 * @author Edis
 */
@ManagedBean
@SessionScoped
public class KorisnickiBean {
    private Korisnik prijavljeniKorisnik;
    private boolean prijavljen;
    
    private String staraLozinka="";
    
    
    public void promijeniPostavke()
    {
        System.out.println("Pozvana je promijeni postavke");   
       
        
          try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch(Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Ne moze se izvršiti prijava","Ne može se izvršiti prijava"));
                    return; 
        }
        MySqlKonektor m = new MySqlKonektor();
        try {
  
               m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
               m.ps = m.con.prepareStatement("UPDATE korisnici SET ime = ?, prezime = ?, korisnicko_ime= ?, lozinka = ?, radnik = ?, klijent = ?, admin = ?  WHERE korisnik_id=?");
               m.ps.setString(1, prijavljeniKorisnik.getIme());
               m.ps.setString(2, prijavljeniKorisnik.getPrezime());
               m.ps.setString(3, prijavljeniKorisnik.getKorisnickoIme()); 
               m.ps.setString(4, prijavljeniKorisnik.getLozinka()); 
               m.ps.setBoolean(5, prijavljeniKorisnik.isRadnik()); 
               m.ps.setBoolean(6, prijavljeniKorisnik.isKlijent());
               m.ps.setBoolean(7, prijavljeniKorisnik.isAdmin());
               m.ps.setInt(8, prijavljeniKorisnik.getKorisnikId()); 
               m.ps.executeUpdate();
               m.ps.close();
               m.con.close();
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Promjene su spremljenje","Promjene su spremljenje"));
              
        } catch(Exception ex) {
               FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Desila se greška","Desila se greška"));

        }
    }
    
    public String pocetnaStranica()
    {
        if (prijavljeniKorisnik.isAdmin())
        {
            return "pocetna?faces-redirect=true";
        }
        else if (prijavljeniKorisnik.isRadnik())
        {
            return "radnikPocetna?faces-redirect=true";
        }
        else if (prijavljeniKorisnik.isKlijent())
        {
            return "klijentPocetna?faces-redirect=true";
        }
        return null;
    }
    
  
    

    public Korisnik getPrijavljeniKorisnik() {
        return prijavljeniKorisnik;
    }

    public void setPrijavljeniKorisnik(Korisnik prijavljeniKorisnik) {
        this.prijavljeniKorisnik = prijavljeniKorisnik;
    }

    public boolean isPrijavljen() {
        return prijavljen;
    }

    public void setPrijavljen(boolean prijavljen) {
        this.prijavljen = prijavljen;
    }

    public String getStaraLozinka() {
        return staraLozinka;
    }

    public void setStaraLozinka(String staraLozinka) {
        this.staraLozinka = staraLozinka;
    }

    
    
}
