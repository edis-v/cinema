/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import baza.Film;
import baza.Karta;
import baza.Kasa;
import baza.Korisnik;
import java.io.Serializable;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import konektor.MySqlKonektor;

/**
 *
 * @author Edis
 */
@ManagedBean
@SessionScoped
public class KartaBean  implements Serializable{
    private List<Karta> karte = new LinkedList<Karta>();
    private List<Karta> filterKarte = null;
    private Karta karta;
    private Film film;
    private Korisnik korisnik;
    private int brojSale;
    private int preostaloMjesta;
    private Kasa kasa;
    private String ispis1,ispis2;
    
    public KartaBean() {
        ucitajKarte();
        ELContext fcCon = FacesContext.getCurrentInstance().getELContext();                
        FilmBean fb = (FilmBean) fcCon.getELResolver().getValue(fcCon, null, "filmBean");
        film=fb.getFilmovi().get(0);
        listener();
        
    }
    
    
    public void podesiIdFilma(int id){
        
        karta=new Karta();
        karta.setFilmId(id);
    }
    
    
    public int prebrojiRezervacije()
    {
         ELContext fcCon = FacesContext.getCurrentInstance().getELContext();                
         KorisnickiBean kb = (KorisnickiBean) fcCon.getELResolver().getValue(fcCon, null, "korisnickiBean");
         int id = kb.getPrijavljeniKorisnik().getKorisnikId();
         int brojRezervacija=0;
         try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              System.out.println("ID sale je: "+film.getSala());
              m.rs = m.stmt.executeQuery("SELECT COUNT(*) FROM karte WHERE film_id ='"+karta.getFilmId()+"' AND klijent_id = '"+id+"' AND rezervacija = 1");
              
              while(m.rs.next()){
                 brojRezervacija=m.rs.getInt(1); 
              }
              m.stmt.close();
              m.con.close();
              
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         System.out.println("Broj rezervacija: "+brojRezervacija);
        return brojRezervacija;
    }
    
    public void napraviRezervaciju() throws ParseException
    {
        java.util.Date v = new Date();
        SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String vf = traziVrijemeFilma(karta.getFilmId());
        java.util.Date p = new Date(smf.parse(vf).getTime());
        System.out.println("T datum: "+v); System.out.println("F datum: "+p);
        if (v.before(p)){
        
        if (prebrojiRezervacije()<2){
        String kod = generisiKod();
        ispis1="Kod vaše rezervacije je: ";
        ispis2="Prilikom dolaska na kasu priložite vaš kod rezervacije.";
        karta.setKod(kod);
        ELContext fcCon = FacesContext.getCurrentInstance().getELContext();                
        KorisnickiBean kb = (KorisnickiBean) fcCon.getELResolver().getValue(fcCon, null, "korisnickiBean");
        karta.setKlijentId(kb.getPrijavljeniKorisnik().getKorisnikId()); 
        snimiKartu(false);
        }
        else {
            ispis1="Kartu nije moguće rezervisati";
            ispis2="Maksimalan broj rezervacija po filmu je 2";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Greška","Greška"));
        }
        
        } else
        {
            ispis1="Kartu nije moguće rezervisati";
            ispis2="Odabrani film je vec počeo";
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Greška","Greška"));
        }
        
    }
    
    public void podesiKartu(Karta k) 
    {
        if (preostaloMjesta>0){
        karta=k;
        snimiKartu(false);
        dodajDnevniPrihod();
        ucitajKarte();
        } else 
        {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Film je rasprodan","Film je rasprodan"));

        }
    }
    
    
    public void prodajaKarte()
            
    {
        if (preostaloMjesta>0){
        karta=new Karta(); System.out.println("Prije filma");
        karta.setFilmId(film.getFilmId()); System.out.println("Poslije filma");
        
        ELContext fcCon = FacesContext.getCurrentInstance().getELContext();                
        KorisnickiBean kb = (KorisnickiBean) fcCon.getELResolver().getValue(fcCon, null, "korisnickiBean"); System.out.println("Poslije elContexta");
        
        karta.setKlijentId(kb.getPrijavljeniKorisnik().getKorisnikId()); 
        
        karta.setRezervacija(false);
        karta.setKod(null);
        Date datum = new Date();
        karta.setDatumProdaje(datum);
        
        KasaBean kasab = (KasaBean) fcCon.getELResolver().getValue(fcCon, null, "kasaBean");
        int duzina = kasab.getDnevnaStanja().size();
        if (duzina>=1){
        snimiKartu(true); System.out.println("Pozivanje funkcije dodajDnevniPrihod()");
        dodajDnevniPrihod(); System.out.println("Funkcija dodajDnevniPrihod() zavrsila");
        } else
        {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Admin mora unijeti početno stanje kase","Admin mora unijeti početno stanje kase"));

        }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Film je rasprodan","Film je rasprodan"));

        }
       
        
    }
    
    
    public void dodajDnevniPrihod() 
    {
        
        ELContext fcCon = FacesContext.getCurrentInstance().getELContext();                
        KasaBean kb = (KasaBean) fcCon.getELResolver().getValue(fcCon, null, "kasaBean");
        System.out.println("Poslije elcontexta");
        int duzina = kb.getDnevnaStanja().size();
       
        System.out.println("Id dnevnog stanja je: "+kb.getDnevnaStanja().get((duzina-1)).getKasaId());
        kasa = kb.getDnevnaStanja().get((duzina-1));
        
        System.out.println("Id prethodne kase je: "+kasa.getKasaId());
        System.out.println("Stanje kase je: "+kasa.getStanje());
        java.util.Date datum = new java.util.Date(kasa.getDatum_zakljucivanja().getTime());
        System.out.println("Datum zadnjeg snimanja je: "+datum);
        Date trenutniDatum = new Date();
        System.out.println("Trenutni datum je: "+trenutniDatum);
        SimpleDateFormat smf = new SimpleDateFormat("yyyy-MM-dd");
        String s1=smf.format(datum);
        String s2=smf.format(trenutniDatum);
        
        String a = traziKorisnika(kasa.getZakljucio(), "admin");
        
         try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch(Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Ne moze se izvršiti prijava","Ne može se izvršiti prijava"));
                    return ; 
        }
        MySqlKonektor m = new MySqlKonektor();
        System.out.println("Pocetak try");
        try {
          m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
            if (s1.equalsIgnoreCase(s2) && a.equalsIgnoreCase("0"))
             {   
                 java.sql.Date sqlDate = new java.sql.Date(kasa.getDatum_zakljucivanja().getTime());
                 System.out.println("Prije setovanja prihoda");
                 kasa.setPrihod(kasa.getPrihod()+film.getCijena());
                 kasa.setStanje(kasa.getStanje()+film.getCijena());
                  System.out.println("Datumi su isti"); System.out.println("Id kase je: "+kasa.getKasaId());
                  m.ps = m.con.prepareStatement("UPDATE kase SET stanje = ?, prihod=?, rashod=?, datum_zakljucivanja=?, zakljucio=?  WHERE kasa_id=?");
                  m.ps.setInt(1, kasa.getStanje());
                  m.ps.setInt(2, kasa.getPrihod());  System.out.println("Dodano je: "+film.getCijena() +" KM");
                  m.ps.setInt(3, kasa.getRashod());
                 m.ps.setDate(4, sqlDate);
                 m.ps.setInt(5, kasa.getZakljucio());
                 m.ps.setInt(6, kasa.getKasaId());
               m.ps.executeUpdate();
               m.ps.close();

             }
            else 
            {
                 System.out.println("Novi insert");
                 //kasa = new Kasa();
                int stanje = kasa.getStanje(); System.out.println("Stanje je: "+stanje);
                kasa= new Kasa();
                ELContext fcCon1 = FacesContext.getCurrentInstance().getELContext();                
                KorisnickiBean korb = (KorisnickiBean) fcCon1.getELResolver().getValue(fcCon1, null, "korisnickiBean");
                kasa.setZakljucio(korb.getPrijavljeniKorisnik().getKorisnikId());
                java.sql.Date sqlDate = new java.sql.Date(trenutniDatum.getTime());
                kasa.setDatum_zakljucivanja(trenutniDatum);
                kasa.setStanje(stanje+film.getCijena());
                kasa.setPrihod(film.getCijena());
                kasa.setRashod(0);
                
               
                m.ps = m.con.prepareStatement("INSERT INTO kase VALUES (?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
                
                m.ps.setInt(1, kasa.getKasaId());
           m.ps.setInt(2, stanje);
           m.ps.setInt(3, film.getCijena());
           m.ps.setInt(4, 0);
           m.ps.setDate(5, sqlDate);
           m.ps.setInt(6, kasa.getZakljucio());
           m.ps.executeUpdate();
           m.rs = m.ps.getGeneratedKeys();
           m.rs.next();
           kasa.setKasaId(m.rs.getInt(1));
           m.rs.close();
           m.ps.close();
           kb.getDnevnaStanja().add(kasa);
            }
            m.con.close();
        }
        catch (Exception ex) {}
        
        }
        
      
    
    public void snimiKartu(boolean prodaja)
    {
          try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch(Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Ne moze se izvršiti prijava","Ne može se izvršiti prijava"));
                    return; 
        }
        MySqlKonektor m = new MySqlKonektor();
        try
        {
           m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
           if (karta.getKartaId() == 0)
           {
           m.ps = m.con.prepareStatement("INSERT INTO karte VALUES (?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
           m.ps.setInt(1, karta.getKartaId());
           m.ps.setInt(2, karta.getFilmId());
           m.ps.setInt(3, karta.getKlijentId());
           if (prodaja==false)
           {
           m.ps.setDate(4, null);
           m.ps.setBoolean(5, true);
           }
           else
           {
            java.sql.Date sqlDate = new java.sql.Date(karta.getDatumProdaje().getTime());
            m.ps.setDate(4, sqlDate);
            m.ps.setBoolean(5, karta.isRezervacija());
           }
           m.ps.setString(6, karta.getKod());
           m.ps.executeUpdate();
           m.rs = m.ps.getGeneratedKeys();
           m.rs.next();
           karta.setKartaId(m.rs.getInt(1));
           m.rs.close();
           m.ps.close();
           if (prodaja==false)
            {
               karte.add(karta);
            }
           
           if (prodaja==false){
           FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Uspješna rezervacija","Uspješna rezervacija"));
           } else
           {
           FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Karta je uspješno naplaćena","Karta je uspješno naplaćena"));

           }
           }
           else
           {
  
               Date datum = new Date();
               java.sql.Date sqlDate = new java.sql.Date(datum.getTime());
               m.ps = m.con.prepareStatement("UPDATE karte SET film_id=?, klijent_id=?, datum_prodaje=?, rezervacija=?, kod=?  WHERE karta_id=?");
               m.ps.setInt(1, karta.getFilmId());
               m.ps.setInt(2, karta.getKlijentId());
               m.ps.setDate(3, sqlDate);
               m.ps.setBoolean(4, false);
               m.ps.setString(5, karta.getKod());
               m.ps.setInt(6, karta.getKartaId());
               m.ps.executeUpdate();
               m.ps.close();
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Karta je naplaćena","Karta je naplaćena"));

           }
           m.con.close();
        }  catch (Exception ex){ 
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Desila se greška","Desila se greška"));

            return;}
      
        preostaloMjesta--;
    }
    
    private void ucitajKarte()
    {
         karte.clear();
         try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return;
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              m.rs = m.stmt.executeQuery("SELECT * FROM karte WHERE rezervacija = 1");
              
              while(m.rs.next()){
                  karte.add(new Karta(m.rs.getInt(1),m.rs.getInt(2), m.rs.getInt(3), m.rs.getDate(4), m.rs.getBoolean(5), m.rs.getString(6)));
              }
              m.stmt.close();
              m.con.close();
            
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         
    }
    
    
    public String traziKorisnika(int id, String trazi)
    {
            try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return null;
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              m.rs = m.stmt.executeQuery("SELECT * FROM korisnici WHERE korisnik_id = "+id);
              
              while(m.rs.next()){
                  korisnik = new Korisnik(m.rs.getInt(1),m.rs.getString(2), m.rs.getString(3), m.rs.getString(4),m.rs.getString(5), m.rs.getBoolean(6),  m.rs.getBoolean(7), m.rs.getBoolean(8));
              }
              m.stmt.close();
              m.con.close();
             
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         
         if (trazi.equalsIgnoreCase("ime"))
         {
             return korisnik.getIme();
         }
         else if (trazi.equalsIgnoreCase("prezime"))
         {
             return korisnik.getPrezime();
         }
         else if (trazi.equalsIgnoreCase("admin"))
         {
             if (korisnik.isAdmin()==true)
             {
                 return "1";
             }
             else if (korisnik.isAdmin()==false)
             {
                 return "0";
             }
         }
         
         return null;
    }
    
    private String generisiKod()
    {
          String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int charactersLength = characters.length();
        StringBuffer buffer = new StringBuffer();
        
        for (int i = 0; i < 5; i++) {
			double index = Math.random() * charactersLength;
			buffer.append(characters.charAt((int) index));  
		}
        return buffer.toString();
    }
    
    public void listener()
    {
        System.out.println("Odabran je film: "+film.getNazivFilma());
        int brojMjesta=0;
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              System.out.println("ID sale je: "+film.getSala());
              m.rs = m.stmt.executeQuery("SELECT * FROM sale WHERE sala_id ='" +film.getSala()+"'");
              
              while(m.rs.next()){
                  brojSale=m.rs.getInt(2); System.out.println("Broj sale je: "+brojSale);
                  brojMjesta=m.rs.getInt(3);
              }
              m.stmt.close();
              m.con.close();
              
             
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         
         preostaloMjesta = brojMjesta - prebrojiKarte(film.getFilmId());
     
         
    }
         
         public int prebrojiKarte(int id)
         {
             int brojac=0;
              try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return 0;
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              m.rs = m.stmt.executeQuery("SELECT * FROM karte WHERE film_id = "+id);
              
              while(m.rs.next()){
                  brojac=brojac+1;
              }
              m.stmt.close();
              m.con.close();
            
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         
         return brojac;
         }
         
       
   
    public String traziVrijemeFilma(int filmId)
    {
        String vrijeme=null;
         try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return null;
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              m.rs = m.stmt.executeQuery("SELECT * FROM filmovi WHERE film_id = " + filmId);
              
             while(m.rs.next()){
                    vrijeme=m.rs.getString(3);
              }
              m.stmt.close();
              m.con.close();
      
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         return vrijeme;
    }
    

    public List<Karta> getKarte() {
        return karte;
    }

    public void setKarte(List<Karta> karte) {
        this.karte = karte;
    }

    public Karta getKarta() {
        return karta;
    }

    public void setKarta(Karta karta) {
        this.karta = karta;
    }

    public List<Karta> getFilterKarte() {
        return filterKarte;
    }

    public void setFilterKarte(List<Karta> filterKarte) {
        this.filterKarte = filterKarte;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }


    public int getBrojSale() {
        return brojSale;
    }

    public void setBrojSale(int brojSale) {
        this.brojSale = brojSale;
    }

    public int getPreostaloMjesta() {
        return preostaloMjesta;
    }

    public void setPreostaloMjesta(int preostaloMjesta) {
        this.preostaloMjesta = preostaloMjesta;
    }

    public Kasa getKasa() {
        return kasa;
    }

    public void setKasa(Kasa kasa) {
        this.kasa = kasa;
    }

    public String getIspis1() {
        return ispis1;
    }

    public void setIspis1(String ispis1) {
        this.ispis1 = ispis1;
    }

    public String getIspis2() {
        return ispis2;
    }

    public void setIspis2(String ispis2) {
        this.ispis2 = ispis2;
    }
    
    
    
}
