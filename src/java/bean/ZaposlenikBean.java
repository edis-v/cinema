/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bean;

import baza.Korisnik;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.LinkedList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import konektor.MySqlKonektor;

/**
 *
 * @author Edis
 */
@ManagedBean
@SessionScoped
public class ZaposlenikBean {
    private List<Korisnik> zaposlenici = new LinkedList<Korisnik>();
    private Korisnik zaposlenik;

    public ZaposlenikBean(){
        ucitajZaposlenike();
    }
    
    
    
    private String ucitajZaposlenike()
    {
        zaposlenici.clear();
         try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
            return null;
        }
         MySqlKonektor m = new MySqlKonektor();
         try{
                m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
              m.stmt = m.con.createStatement();
              m.rs = m.stmt.executeQuery("SELECT * FROM korisnici WHERE radnik=1");
              
              while(m.rs.next()){
                  zaposlenici.add(new Korisnik(m.rs.getInt(1),m.rs.getString(2), m.rs.getString(3), m.rs.getString(4),m.rs.getString(5), m.rs.getBoolean(6),  m.rs.getBoolean(7), m.rs.getBoolean(8)));
              }
              m.stmt.close();
              m.con.close();
             
         }
         catch(Exception ex){
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Pogreška u bazi!!!", "Pogreška u bazi!!!"));
         }
         
        return null;
    }
    
    public String kreirajZaposlenika()
    {
        zaposlenik=new Korisnik();
        return null;
    }
    
    public String snimiZaposlenika()
    {
         try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch(Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Ne moze se izvršiti prijava","Ne može se izvršiti prijava"));
                    return null; 
        }
        MySqlKonektor m = new MySqlKonektor();
        try
        {
           m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
           if (zaposlenik.getKorisnikId()==0)
           {
              m.ps = m.con.prepareStatement("INSERT INTO korisnici VALUES (?,?,?,?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
              m.ps.setInt(1, zaposlenik.getKorisnikId());
              m.ps.setString(2, zaposlenik.getIme());
              m.ps.setString(3, zaposlenik.getPrezime());
              m.ps.setString(4, zaposlenik.getKorisnickoIme());
              m.ps.setString(5, zaposlenik.getLozinka());
              m.ps.setBoolean(6, true);
              m.ps.setBoolean(7, false);
              m.ps.setBoolean(8, false);
              m.ps.executeUpdate();
              m.rs = m.ps.getGeneratedKeys();
              m.rs.next();
              zaposlenik.setKorisnikId(m.rs.getInt(1));
              m.rs.close();
              m.ps.close();
              zaposlenici.add(zaposlenik);
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Uspješan unos","Uspješan unos"));
            
            }
            else
             {
               m.ps = m.con.prepareStatement("UPDATE korisnici SET ime = ?, prezime=?, korisnicko_ime=?, lozinka=?, radnik=?, klijent=?, admin=?  WHERE korisnik_id=?");
               m.ps.setString(1, zaposlenik.getIme());
               m.ps.setString(2, zaposlenik.getPrezime());
               m.ps.setString(3, zaposlenik.getKorisnickoIme());
               m.ps.setString(4, zaposlenik.getLozinka());
               m.ps.setBoolean(5, zaposlenik.isRadnik());
               m.ps.setBoolean(6, zaposlenik.isKlijent());
               m.ps.setBoolean(7, zaposlenik.isAdmin());
               m.ps.setInt(8, zaposlenik.getKorisnikId());
               m.ps.executeUpdate();
               m.ps.close();
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Uspješan unos","Napravljene su promjene u bazi"));

             }
           
        } catch (Exception ex) {
              FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Desila se greška","Desila se greška"));
        }
        return null;
    }
    
    
    public String urediZaposlenika(Korisnik z)
    {
        zaposlenik=z; 
        return null;
    }
    
    public String obrisiZaposlenika()
    {
            try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch(Exception ex) {
          FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Ne može se izvršiti prijava","Ne može se izvršiti prijava"));
           return null; 
        }
         MySqlKonektor m = new MySqlKonektor();
         try
         {
            m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
            m.stmt=m.con.createStatement();
            m.stmt.execute("DELETE FROM korisnici WHERE korisnik_id = "+zaposlenik.getKorisnikId());
            m.stmt.close();
            m.con.close();
         }catch (Exception ex)
         {
             FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Desila se greška","Desila se greška"));
             return null;
         }
         zaposlenici.remove(zaposlenik);
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Zaposlenik je obrisan ","Zaposlenik je obrisan"));

            return null;
    }
    
    public void postaviZaposlenikaZaBrisanje(Korisnik z)
    {
        zaposlenik=z; System.out.println("Zaposlenik: "+zaposlenik.getIme());
    }
    
    
    public List<Korisnik> getZaposlenici() {
        return zaposlenici;
    }

    public void setZaposlenici(List<Korisnik> zaposlenici) {
        this.zaposlenici = zaposlenici;
    }

    public Korisnik getZaposlenik() {
        return zaposlenik;
    }

    public void setZaposlenik(Korisnik zaposlenik) {
        this.zaposlenik = zaposlenik;
    }
    
    
    
    
    
    
}
