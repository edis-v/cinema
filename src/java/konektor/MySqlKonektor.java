/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package konektor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Edis
 */
public class MySqlKonektor {
    public static final String USER = "root";
    public static final String PASSWORD = "root";
    public static final String DB_NAME = "kino";
    public static final int DB_PORT = 3306;
    public static final String DB_URL = "jdbc:mysql://localhost:" + DB_PORT + "/" + DB_NAME;
    
    public java.sql.Connection con;
    public Statement stmt;
    public ResultSet rs;
    public PreparedStatement ps;
}
