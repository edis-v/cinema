/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package konvertori;

import baza.Film;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Edis
 */
@FacesConverter(value = "filmKonvertor")
public class FilmKonvertor implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String id) {
        for(UIComponent component : uic.getChildren())
        {
            if(component.getId().equals("filmovi") && component instanceof UISelectItems)
            {
                UISelectItems items = (UISelectItems) component;
                List<Film> listaFilmova = (List<Film>) items.getValue();
                for(Film f : listaFilmova)
                {
                    if(id.equals(f.getFilmId() + ""))
                    {
                        return f;
                    }
                }
            }
        }        
        throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_FATAL, "Greška u konverziji1.", "Greška u konverziji1."));
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
          if(o != null && o instanceof Film)
        {            
            Film f = (Film) o;
            return f.getFilmId() + "";
        }
        else
        {
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_FATAL, "Greška u konverziji.", "Greška u konverziji."));
        }
    }
    
    }
    

