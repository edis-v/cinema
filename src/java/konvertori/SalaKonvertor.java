/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package konvertori;

import baza.Sala;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Edis
 */
@FacesConverter(value = "salaKonvertor")
public class SalaKonvertor implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String id) {
        for(UIComponent component : uic.getChildren())
        {
            if(component.getId().equals("sale") && component instanceof UISelectItems)
            {
                UISelectItems items = (UISelectItems) component;
                List<Sala> listaSala = (List<Sala>) items.getValue();
                for(Sala s : listaSala)
                {
                    if(id.equals(s.getSalaId() + ""))
                    {
                        return s;
                    }
                }
            }
        }        
        throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_FATAL, "Greška u konverziji1.", "Greška u konverziji1."));
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
         if(o != null && o instanceof Sala)
        {            
            Sala s = (Sala) o;
            return s.getSalaId() + "";
        }
        else
        {
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_FATAL, "Greška u konverziji.", "Greška u konverziji."));
        }
    }
    
}
