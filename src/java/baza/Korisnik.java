/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package baza;

import java.io.Serializable;

/**
 *
 * @author Edis
 */
public class Korisnik implements Serializable{
    private int korisnikId;
    private String ime,prezime,korisnickoIme,lozinka;
    private boolean radnik,admin,klijent;
    
    public Korisnik (){}

    public Korisnik(int korisnikId, String ime, String prezime, String korisnickoIme, String lozinka, boolean radnik, boolean klijent, boolean admin) {
        this.korisnikId = korisnikId;
        this.ime = ime;
        this.prezime = prezime;
        this.korisnickoIme = korisnickoIme;
        this.lozinka = lozinka;
        this.radnik = radnik;
        this.admin = admin;
        this.klijent = klijent;
    }

    
    
    
    public int getKorisnikId() {
        return korisnikId;
    }

    public void setKorisnikId(int korisnikId) {
        this.korisnikId = korisnikId;
    }

    public boolean isRadnik() {
        return radnik;
    }

    public void setRadnik(boolean radnik) {
        this.radnik = radnik;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isKlijent() {
        return klijent;
    }

    public void setKlijent(boolean klijent) {
        this.klijent = klijent;
    }
    
    

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }
    
    
}
