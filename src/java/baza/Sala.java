/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package baza;

import java.io.Serializable;

/**
 *
 * @author Edis
 */
public class Sala implements Serializable{
    private int salaId,brojMjesta,brojSale;
    
    public Sala() {}

    public Sala(int salaId, int brojSale, int brojMjesta) {
        this.salaId = salaId;
        this.brojMjesta = brojMjesta;
        this.brojSale = brojSale;
    }
    
    
    

    public int getSalaId() {
        return salaId;
    }

    public void setSalaId(int salaId) {
        this.salaId = salaId;
    }

    public int getBrojMjesta() {
        return brojMjesta;
    }

    public void setBrojMjesta(int brojMjesta) {
        this.brojMjesta = brojMjesta;
    }

    public int getBrojSale() {
        return brojSale;
    }

    public void setBrojSale(int brojSale) {
        this.brojSale = brojSale;
    }
    
    
    
}
