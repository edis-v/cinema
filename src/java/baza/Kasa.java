/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package baza;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Edis
 */
public class Kasa implements Serializable{
    private int kasaId, stanje,prihod,rashod,zakljucio;
    private Date datum_zakljucivanja;
    
    public Kasa(){}

    public Kasa(int kasaId, int stanje, int prihod, int rashod, Date datum_zakljucivanja, int zakljucio) {
        this.kasaId = kasaId;
        this.stanje = stanje;
        this.prihod = prihod;
        this.rashod = rashod;
        this.zakljucio = zakljucio;
        this.datum_zakljucivanja = datum_zakljucivanja;
    }
    
    
    

    public int getKasaId() {
        return kasaId;
    }

    public void setKasaId(int kasaId) {
        this.kasaId = kasaId;
    }

    public int getStanje() {
        return stanje;
    }

    public void setStanje(int stanje) {
        this.stanje = stanje;
    }

    public int getPrihod() {
        return prihod;
    }

    public void setPrihod(int prihod) {
        this.prihod = prihod;
    }

    public int getRashod() {
        return rashod;
    }

    public void setRashod(int rashod) {
        this.rashod = rashod;
    }

    public int getZakljucio() {
        return zakljucio;
    }

    public void setZakljucio(int zakljucio) {
        this.zakljucio = zakljucio;
    }

    public Date getDatum_zakljucivanja() {
        return datum_zakljucivanja;
    }

    public void setDatum_zakljucivanja(Date datum_zakljucivanja) {
        this.datum_zakljucivanja = datum_zakljucivanja;
    }
    
    
    
    
}
