/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package baza;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Edis
 */
public class Karta implements Serializable{
    private int kartaId,klijentId,filmId;
    private String kod;
    private boolean rezervacija;
    private Date datumProdaje;

    public Karta() {}
    
    public Karta(int kartaId, int filmId, int klijentId,  Date datumProdaje,  boolean rezervacija, String kod) {
        this.kartaId = kartaId;
        this.klijentId = klijentId;
        this.filmId = filmId;
        this.kod = kod;
        this.rezervacija = rezervacija;
        this.datumProdaje = datumProdaje;
    }
    
    
    

    public int getKartaId() {
        return kartaId;
    }

    public void setKartaId(int kartaId) {
        this.kartaId = kartaId;
    }

    public int getKlijentId() {
        return klijentId;
    }

    public void setKlijentId(int klijentId) {
        this.klijentId = klijentId;
    }

    public int getFilmId() {
        return filmId;
    }

    public void setFilmId(int filmId) {
        this.filmId = filmId;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public boolean isRezervacija() {
        return rezervacija;
    }

    public void setRezervacija(boolean rezervacija) {
        this.rezervacija = rezervacija;
    }

    public Date getDatumProdaje() {
        return datumProdaje;
    }

    public void setDatumProdaje(Date datumProdaje) {
        this.datumProdaje = datumProdaje;
    }
    
    
    
}
