/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package validatori;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import konektor.MySqlKonektor;

/**
 *
 * @author Edis
 */
@FacesValidator(value = "brojSaleValidator")
public class BrojSaleValidator implements Validator{

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
         boolean postoji = false;
        int brojSale = 0;
        if (o!=null)
        {
            try{
                brojSale=Integer.parseInt(o.toString());
            }catch(Exception ex){
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nije unešena brojčana vrijednost.", "Nije unešena brojčana vrijednost."));
            }
            
            
            try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
        }
         MySqlKonektor m = new MySqlKonektor();
         try {
             m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
            m.stmt = m.con.createStatement();
            m.rs = m.stmt.executeQuery("SELECT * from sale");
            while (m.rs.next())
            {
                if (brojSale==(m.rs.getInt(2)))
                {
                    postoji=true;
                    break;
                }
            }
             
         }  catch (SQLException ex) {
                Logger.getLogger(KorisnickoImeValidator.class.getName()).log(Level.SEVERE, null, ex);
            }
         
         
         if (postoji==true)
         {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Broj sale je zauzet.", "Broj sale je zauzet."));
         }
         
        }
        else {
          throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Vrijednost je NULL.", "Vrijednost je NULL."));

        }
    }
    
    }
    
