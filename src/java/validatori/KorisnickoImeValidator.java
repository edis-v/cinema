/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package validatori;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import konektor.MySqlKonektor;

/**
 *
 * @author Edis
 */
@FacesValidator(value = "korisnickoImeValidator")
public class KorisnickoImeValidator implements Validator{

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
        boolean postoji = false;
        String kIme = null;
        if (o!=null && o instanceof String)
        {
            try{
                kIme=o.toString();
            }catch(Exception ex){
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_FATAL, "Nije unešena vrijednost.", "Nije unešena vrijednost."));
            }
            
            
            try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ne može izvršiti prijava!!!", "Ne može izvršiti prijava!!!"));
        }
         MySqlKonektor m = new MySqlKonektor();
         try {
             m.con = DriverManager.getConnection(MySqlKonektor.DB_URL, MySqlKonektor.USER, MySqlKonektor.PASSWORD); //uspostavljanje konekcije
            m.stmt = m.con.createStatement();
            m.rs = m.stmt.executeQuery("SELECT * from korisnici");
            while (m.rs.next())
            {
                if (kIme.equalsIgnoreCase(m.rs.getString(4)))
                {
                    postoji=true;
                    break;
                }
            }
             
         }  catch (SQLException ex) {
                Logger.getLogger(KorisnickoImeValidator.class.getName()).log(Level.SEVERE, null, ex);
            }
         
         
         if (postoji==true)
         {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Korisničko ime je zauzeto.", "Korisničko ime je zauzeto."));
         }
         
        }
        else {
          throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Vrijednost je NULL.", "Vrijednost je NULL."));

        }
    }
    
}
