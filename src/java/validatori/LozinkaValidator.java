/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package validatori;

import bean.KorisnickiBean;
import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Edis
 */
@FacesValidator(value="lozinkaValidator")
public class LozinkaValidator implements Validator{

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object o) throws ValidatorException {
        boolean postoji=false;
        if (o!=null && o instanceof String)
        {
             ELContext fcCon = FacesContext.getCurrentInstance().getELContext();
             KorisnickiBean kb = (KorisnickiBean) fcCon.getELResolver().getValue(fcCon, null, "korisnickiBean");
             System.out.println("Lozinka je: "+kb.getPrijavljeniKorisnik().getLozinka());
             System.out.println("Object lozinka je: "+o.toString());
             if(o.toString().equalsIgnoreCase(kb.getPrijavljeniKorisnik().getLozinka()))
             {
                 postoji=true;
             }
             
             if(postoji==false)
             {
               throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Lozinka nije validna.", "Lozinka nije validna."));
             }
        }
        else
        {
             throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_FATAL, "Vrijednost je NULL", "Vrijednost je NULL."));
        }
    }
    
}
